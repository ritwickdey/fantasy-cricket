def calculate_score(match_data):
    score = 0
    score += 1 if match_data['Scored'] >= 2 else 0
    score += 5 if 50 <= match_data['Scored'] < 100 else 0
    score += 10 if match_data['Scored'] >= 100 else 0

    strike_rate = match_data['Scored'] / match_data['Faced'] * 100 if match_data['Faced'] != 0 else 0
    score += 2 if 80 <= strike_rate <= 100 else 0
    score += 4 if strike_rate > 100 else 0
    score += 1 if match_data['fours'] >= 1 else 0
    score += 2 if match_data['sixes'] >= 1 else 0

    score += match_data['wkts'] * 10
    score += 5 if match_data['wkts'] == 3 else 0
    score += 10 if match_data['wkts'] >= 5 else 0
    total_over = match_data['bowled'] / 6 if match_data['bowled'] is not 0 else 0
    economy_rate = match_data['given'] / total_over if total_over is not 0 else 0
    score += 4 if 3.5 < economy_rate <= 4.5 else 0
    score += 7 if 2 < economy_rate <= 3.5 else 0
    score += 10 if 0 < economy_rate <= 2 else 0

    score += match_data['catches'] * 10
    score += match_data['stumping'] * 10
    score += match_data['ro'] * 10

    return score
