from PyQt5.QtSql import QSqlDatabase


class Database:
    def __init__(self):
        self.db = self.__connect_db__()

    def __connect_db__(self):
        db = QSqlDatabase.addDatabase('QSQLITE')
        db.setDatabaseName('./DB/cricket.db')
        db.open()
        return db

    def get_players(self):
        return self.db.exec_('select id, name, Value, CategoryId  from players join PlayerDetails on '
                             'PlayerDetails.PlayerId = Players.Id')

    def set_team(self, name):
        return self.db.exec_("INSERT into Teams(name) values('{0}')".format(name))  # I know it is very bad

    def get_all_team(self):
        return self.db.exec_("SELECT id,name from teams")

    def get_team(self, name):
        return self.db.exec_("SELECT id,name from teams where name = '{0}'".format(name))

    def get_matches(self):
        return self.db.exec_("SELECT id,title from matches")

    def get_players_by_category(self, category_id):
        return self.db.exec_('select id, name, Value, CategoryId  from players join PlayerDetails on '
                             'PlayerDetails.PlayerId = Players.Id '
                             'where categoryId = %s' % category_id)

    def set_team_members(self, team_id, team_members_ids):
        self.db.exec('DELETE from TeamMembers where TeamId = {0} '.format(team_id))
        for team_member_id in team_members_ids:
            print('INSERT into TeamMembers(TeamId, PlayerId) values({0}, {1})'.format(team_id, team_member_id))
            self.db \
                .exec('INSERT into TeamMembers(TeamId, PlayerId) values({0}, {1})'.format(team_id, team_member_id))

    def get_team_members(self, team_id):
        return self.db.exec_('select T3.name as PlayerName, T2.name as TeamName,TeamId, PlayerId '
                             'from TeamMembers as T1 '
                             'inner join Teams as T2 '
                             'on T2.id = T1.TeamId '
                             'inner join Players as T3 '
                             'on T1.PlayerId = T3.Id '
                             'where T1.TeamId = {0}'.format(team_id))

    def get_match_details_by_player(self, match_id, player_id):
        return self.db.exec_('select Scored, Faced, fours, sixes, bowled, maiden, given, wkts, '
                             'catches, stumping, ro, totalMatches, totalRuns, total100s, total50s '
                             'from MatchDetails '
                             'natural join PlayerDetails '
                             'where matchId = {0} and playerId = {1}'.format(match_id, player_id))

    def close_db(self):
        if self.db.isOpen():
            self.db.close()
