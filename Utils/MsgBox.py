from PyQt5.QtWidgets import QMessageBox


def message_box(text, icon=QMessageBox.Information, title='Message'):
    msg = QMessageBox()
    msg.setIcon(icon)
    msg.setText(text)
    msg.setWindowTitle(title)
    msg.setStandardButtons(QMessageBox.Ok)
    return msg
