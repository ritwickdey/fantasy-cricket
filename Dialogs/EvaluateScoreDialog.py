from functools import reduce

from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QMessageBox
from PyQt5.uic import loadUi

from Utils.Database import Database
from Utils.MsgBox import message_box
from Utils.calculate_score import calculate_score


class EvaluateScoreDialog(QDialog):
    matches = []
    teams = []
    teams_members = []
    selected_team = None
    selected_match = None
    selected_match_details = []
    calculated_scores = []

    def __init__(self):
        super(EvaluateScoreDialog, self).__init__()
        loadUi('./UI/EvaluateScore.ui', self)
        self.db = Database()
        self.init_app()

    def init_app(self):
        self.fetch_matches()
        self.fetch_teams()
        self.matches_cb.addItem('Select Match')
        self.teams_cb.addItem('Select Team')
        self.matches_cb.addItems(map(lambda x: x['title'], self.matches))
        self.teams_cb.addItems(map(lambda x: x['name'], self.teams))

        self.teams_cb.currentIndexChanged.connect(self.on_team_selected)
        self.matches_cb.currentIndexChanged.connect(self.on_match_selected)
        self.score_btn.clicked.connect(self.fetch_and_calculate_score)

    def fetch_teams(self):
        self.teams = []
        result = self.db.get_all_team()
        while result.next():
            self.teams.append({'id': result.value('id'), 'name': result.value('name')})

    def fetch_matches(self):
        result = self.db.get_matches()
        while result.next():
            self.matches.append({'id': result.value('id'), 'title': result.value('title')})

    def on_team_selected(self, i):
        self.selected_team = self.teams[i - 1] if i is not 0 else None
        self.load_player_list_view()

    def fetch_and_calculate_score(self):
        try:
            if self.selected_team is None or self.selected_match is None:
                message_box('Please select Team and Match to proceed',
                            title='Error',
                            icon=QMessageBox.Critical).exec_()
                return
            self.selected_match_details = []
            self.calculated_scores = []
            for member in self.teams_members:
                match_id, player_id = self.selected_match['id'], member['PlayerId']
                result = self.db.get_match_details_by_player(match_id, player_id)
                if result.next():
                    match_data = self.map_result_to_match_details(result)
                    self.selected_match_details.append(match_data)
                    self.calculated_scores.append(calculate_score(match_data))

            self.load_score_list_view()
            print(self.selected_match_details)

        except Exception as e:
            print(e)

    def load_player_list_view(self):
        if self.player_list_view.model() is not None:
            self.player_list_view.model().removeRows(0, self.player_list_view.model().rowCount())
        self.load_team_members()
        model = QStandardItemModel(self.player_list_view)
        for member in self.teams_members:
            item = QStandardItem(member['PlayerName'])
            item.setEditable(False)
            item.setSelectable(False)
            model.appendRow(item)
        self.player_list_view.setModel(model)

    def load_score_list_view(self):
        if self.score_list_view.model() is not None:
            self.score_list_view.model().removeRows(0, self.score_list_view.model().rowCount())
        model = QStandardItemModel(self.score_list_view)
        for score in self.calculated_scores:
            item = QStandardItem(str(score))
            item.setEditable(False)
            item.setSelectable(False)
            model.appendRow(item)
        self.score_list_view.setModel(model)
        self.load_total_score_label()

    def load_total_score_label(self):
        total_score = reduce(lambda pre, now: pre + now, self.calculated_scores, 0)
        self.total_score_label.setText(str(total_score if total_score is not 0 else '###'))

    def on_match_selected(self, i):
        self.selected_match = self.matches[i - 1] if i is not 0 else None
        self.calculated_scores = []
        self.load_score_list_view()

    def load_team_members(self):
        if self.selected_team is None:
            return
        result = self.db.get_team_members(self.selected_team['id'])
        self.teams_members = []
        while result.next():
            self.teams_members.append({
                'PlayerName': result.value('PlayerName'),
                'TeamName': result.value('TeamName'),
                'TeamId': result.value('TeamId'),
                'PlayerId': result.value('PlayerId')
            })

    @staticmethod
    def map_result_to_match_details(result):
        return {
            'Scored': result.value('Scored'),
            'Faced': result.value('Faced'),
            'fours': result.value('fours'),
            'sixes': result.value('sixes'),
            'bowled': result.value('bowled'),
            'maiden': result.value('maiden'),
            'given': result.value('given'),
            'wkts': result.value('wkts'),
            'catches': result.value('catches'),
            'stumping': result.value('stumping'),
            'ro': result.value('ro'),
            'totalMatches': result.value('totalMatches'),
            'totalRuns': result.value('totalRuns'),
            'total100s': result.value('total100s'),
            'total50s': result.value('total50s')
        }
