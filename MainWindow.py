import sys
from functools import reduce

from PyQt5.QtGui import QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
from PyQt5.QtWidgets import QInputDialog
from PyQt5.uic import loadUi

from Dialogs.EvaluateScoreDialog import EvaluateScoreDialog
from Utils.Database import Database
from Utils.MsgBox import message_box


class MainWindow(QMainWindow):
    all_players = [[], [], [], []]
    selected_players = [[], [], [], []]
    total_point = 1000
    used_point = 0
    no_of_bats, no_of_bows, no_of_ar, no_of_wk = 0, 0, 0, 0
    team_details = {'name': None, 'id': None}

    def __init__(self):
        super(MainWindow, self).__init__()
        loadUi('./UI/MainWindow.ui', self)
        self.db = Database()
        self.category_checkboxes = [self.BatCheckBox, self.BowCheckBox, self.ArCheckBox, self.WkCheckBox]
        self.init_app()

        # self.on_evaluate_action_clicked()

    def init_app(self):
        self.initial_app_state()
        self.PlayerlistView.doubleClicked.connect(self.on_player_list_click)
        self.SelectedPlayerListView.doubleClicked.connect(self.on_selected_player_list_click)
        self.action_save.triggered.connect(self.on_save_action_clicked)
        self.action_new.triggered.connect(self.on_new_action_clicked)
        self.action_open.triggered.connect(self.on_open_action_clicked)
        self.action_reset.triggered.connect(self.app_reset)
        self.action_evaluate.triggered.connect(self.on_evaluate_action_clicked)

    def initial_app_state(self):
        self.all_players = [[], [], [], []]
        self.selected_players = [[], [], [], []]

        result = self.db.get_players()
        while result.next():
            self.all_players[result.value('CategoryId') - 1] \
                .append([result.value('id'),
                         result.value('name'),
                         result.value('Value'),
                         result.value('CategoryId')])

        self.total_point = 1000
        self.used_point = 0
        self.no_of_bats, self.no_of_bows, self.no_of_ar, self.no_of_wk = 0, 0, 0, 0
        self.team_details = {'name': None, 'id': None}

    def app_ready_state(self):
        for ch_box in self.category_checkboxes:
            ch_box.setCheckable(True)
        self.category_checkboxes[0].setChecked(True)
        self.set_category_checkbox_events()
        self.load_players_view()
        self.update_points_view()
        self.update_total_players_view()
        self.update_team_name_view()

    def load_players_view(self):
        if self.PlayerlistView.model() is not None:
            self.PlayerlistView.model().removeRows(0, self.PlayerlistView.model().rowCount())

        category_id = self.get_selected_category_id()
        model = QStandardItemModel(self.PlayerlistView)
        for player in self.all_players[category_id - 1]:
            item = QStandardItem(player[1])
            item.setEditable(False)
            item.setSelectable(True)
            model.appendRow(item)
        self.PlayerlistView.setModel(model)

    def load_selected_players_view(self):
        model = QStandardItemModel(self.SelectedPlayerListView)
        for player in list(reduce(lambda prev, now: prev + now, self.selected_players, [])):
            item = QStandardItem(player[1])
            item.setEditable(False)
            item.setSelectable(True)
            model.appendRow(item)
        self.SelectedPlayerListView.setModel(model)

    def on_player_list_click(self, data):
        index, category_index = data.row(), self.get_selected_category_id() - 1
        player = self.all_players[category_index][index]
        if not self.is_possible_to_add_player(player):
            return
        try:
            self.increment_selected_player(player)
            self.selected_players[category_index].append(player)
            self.all_players[category_index].remove(player)
            self.load_players_view()
            self.load_selected_players_view()
            self.update_points_view()
            self.update_total_players_view()
        except Exception as e:
            print(e)

    def on_selected_player_list_click(self, data):
        try:
            player_name = data.data()
            category_index, index = next(
                [i, j] for i in range(len(self.selected_players)) for j in range(len(self.selected_players[i]))
                if self.selected_players[i][j][1] == player_name
            )
            player = self.selected_players[category_index][index]
            self.all_players[category_index].append(player)
            self.selected_players[category_index].remove(player)
            self.increment_selected_player(player, increment_by=-1)
            self.load_players_view()
            self.load_selected_players_view()
            self.update_points_view()
            self.update_total_players_view()
        except Exception as e:
            print(e)

    def get_selected_category_id(self):
        for i in range(len(self.category_checkboxes)):
            if self.category_checkboxes[i].isChecked():
                return i + 1

    def set_category_checkbox_events(self):
        for checkbox in self.category_checkboxes:
            checkbox.toggled.connect(lambda x: self.load_players_view() if x else 0)

    def get_remaining_point(self):
        self.used_point = reduce(lambda prev, now: prev + now[2],
                                 reduce(lambda prev, now: prev + now, self.selected_players, []), 0)
        return self.total_point - self.used_point

    def update_points_view(self):
        remaining_points = self.get_remaining_point()
        self.remaining_point_label.setText(str(remaining_points))
        self.used_point_label.setText(str(self.total_point - remaining_points))

    def update_total_players_view(self):
        self.bat_label.setText(str(self.no_of_bats))
        self.bow_label.setText(str(self.no_of_bows))
        self.ar_label.setText(str(self.no_of_ar))
        self.wk_label.setText(str(self.no_of_wk))

    def increment_selected_player(self, player, increment_by=1):
        category_id = player[3]
        if category_id is 1:
            self.no_of_bats += increment_by
        elif category_id is 2:
            self.no_of_bows += increment_by
        elif category_id is 3:
            self.no_of_ar += increment_by
        elif category_id is 4:
            self.no_of_wk += increment_by

    def is_possible_to_add_player(self, player):
        if self.used_point + player[2] > self.total_point:
            message_box('Insufficient Point.', title='Error', icon=QMessageBox.Critical).exec_()
            return False
        if player[3] is 4 and self.no_of_wk + 1 > 1:
            message_box('Wicket Keeper can\'t be more than one', title='Error', icon=QMessageBox.Critical).exec_()
            return False
        return True

    def update_team_name_view(self):
        self.team_label.setText(self.team_details['name'])

    def on_new_action_clicked(self):
        team_name, ok = QInputDialog.getText(self, 'Team Name', 'Enter your Team Name:')
        if ok:
            try:
                self.db.set_team(team_name)
                result = self.db.get_team(team_name)
                if result.next():
                    self.team_details['id'], self.team_details['name'] = result.value('id'), result.value('name')
                    self.app_ready_state()
            except Exception as ex:
                print(ex)
                message_box('Name exists or Something went wrong!', title='Error', icon=QMessageBox.Critical).exec_()

    def on_save_action_clicked(self):
        if self.team_details['id'] is None or (self.no_of_bats + self.no_of_bows +self.no_of_ar + self.no_of_wk) < 11:
            return message_box('Create/Open Team & a Team should have at least 11 members',
                               title='Error', icon=QMessageBox.Critical).exec_()
        try:
            self.db.set_team_members(
                self.team_details['id'],
                list(map(lambda x: x[0], reduce(lambda prev, now: prev + now, self.selected_players, [])))
            )
            message_box('Your Team is saved', title='Success', icon=QMessageBox.Information).exec_()
            self.app_reset()
        except Exception as ex:
            print(ex)
            message_box('Something went wrong!', title='Error', icon=QMessageBox.Critical).exec_()

    def on_open_action_clicked(self):
        result = self.db.get_all_team()
        teams = []
        while result.next():
            teams.append({'name': result.value('name'), 'id': result.value('id')})
        team_name, ok = QInputDialog.getItem(self, "Teams", "Choose Team", map(lambda x: x['name'], teams), 0, False)
        if ok:
            self.team_details = next(t for t in teams if t['name'] == team_name)
            self.app_ready_state()

    def on_evaluate_action_clicked(self):
        evaluateScoreDialog = EvaluateScoreDialog()
        evaluateScoreDialog.exec_()

    def app_reset(self):
        self.initial_app_state()
        self.remaining_point_label.setText('###')
        self.team_label.setText('###')
        self.used_point_label.setText('###')
        self.bat_label.setText('#')
        self.ar_label.setText('#')
        self.wk_label.setText('#')
        self.bow_label.setText('#')
        for ch_box in self.category_checkboxes:
            ch_box.setCheckable(False)

        if self.PlayerlistView.model() is not None:
            self.PlayerlistView.model().removeRows(0, self.PlayerlistView.model().rowCount())

    def closeEvent(self, *args, **kwargs):
        self.db.close_db()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())
